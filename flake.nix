{
  description = "fpm2nix flake";

  inputs = {
    #
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    # why not?
    flake-utils.url = "github:numtide/flake-utils";

    # easy way to set up some tidy commands
    devshell.url = "github:numtide/devshell";

    # since I personally dont use it for pre-commits, just for the classic `nix flake check` ^^
    check-hooks.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = { self, nixpkgs, flake-utils, devshell, check-hooks }:
    rec {
      overlay = (final: prev: {
        # TODO: package fpm
        #fpm = prev.callPackage ./pkgs/fpm { };
        fpm = { };

        # TODO: implement fpm2nix
        fpm2nix = { };
      });
    } // (flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            self.overlay
            devshell.overlay
          ];
        };
      in
      rec {
        checks = {
          hooks = check-hooks.lib.${system}.run {
            src = ./.;
            hooks = {
              nixpkgs-fmt.enable = true;
            };
          };
        };

        devShell = pkgs.devshell.mkShell {
          name = "fpm2nix";
          packages = with pkgs; [ ];

          commands = [
            {
              name = "fmt";
              help = "Autoformat Nix files";
              command = "${pkgs.nixpkgs-fmt}/bin/nixpkgs-fmt \${@} $PRJ_ROOT";
              category = "dev";
            }

            {
              name = "evalnix";
              help = "Check Nix parsing";
              command = "${pkgs.fd}/bin/fd --extension nix --exec nix-instantiate --parse --quiet {} >/dev/null";
              category = "dev";
            }
          ];
        };

        packages = {
          # TODO
          #inherit (pkgs) fpm;

          # CLI is very very WIP
          #fpm2nix = pkgs.fpm2nix.cli;
        };

        #defaultPackage = packages.fpm;
        #defaultPackage = packages.fpm2nix;

        apps = {
          #fpm = flake-utils.lib.mkApp {
          #drv = packages.fpm;
          #};
          #fpm2nix = flake-utils.lib.mkApp {
          #drv = packages.fpm2nix;
          #};
        };

        #defaultApp = apps.fpm;
        ##defaultApp = apps.fpm2nix;
      }));
}
