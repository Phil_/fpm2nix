**Disclaimer: This project is not even in alpha yet, most if not all functionality still needs to be implemented. It is NOT usable yet; However I highly encourage you to consider contributing. Thanks!**

# fpm2nix

fpm2nix will eventually turn [fpm](https://github.com/fortran-lang/fpm) projects into Nix derivations
without the need to actually write Nix expressions. It does so by parsing fpm.toml (and the planned fpm.lock or similar)
and converting it to Nix derivations on the fly.

## API

The fpm2nix public API currently doesn't consist of any attributes unfortunately

# Using the flake

> See disclaimer at the top

## Contributing

Contributions to this project are *very very* welcome.

Before contributing please do consider running
```sh
nix flake check
```

and 

```sh
fmt
```

to properly format nix files.

## Q & A

### Is this project inspired by [poetry2nix](https://github.com/nix-community/poetry2nix)?

Just slightly, how did you guess?

### Do you have a roadmap?

Kind of, see [./TODO.md](./TODO.md) for list of planned features.

## License

fpm2nix is released under the terms of the MIT license.
